# CS231n-DSTU-Panasenko
# This is header 1

## This is header 2

**Bold** with asterisks.
~~Strikethrough~~ uses two tildes.

## This is another header 2

1. First ordered list item
2. Another item
  * Unordered sub-list item
1. Actual numbers don't matter

Awesome commit message

This commit is related to #17 and fixes #18, #19.
Also Fix #20, Fixes #21 and Closes group/otherproj#22



